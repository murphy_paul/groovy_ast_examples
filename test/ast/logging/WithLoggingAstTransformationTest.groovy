package ast.logging

import org.codehaus.groovy.ast.MethodNode

import java.lang.reflect.Method

/**
 * Created with IntelliJ IDEA.
 * User: paul
 * Date: 20/09/13
 * Time: 6:53 AM
 * To change this template use File | Settings | File Templates.
 */
class WithLoggingAstTransformationTest extends GroovyTestCase {

    public void testWithLoggingClass(){
        def tester = new GroovyClassLoader().parseClass('''
            import ast.logging.SomeService

            class MyClass{

                SomeService someService = new SomeService()

                @ast.logging.WithLogging("1")
                public void doLogging(){
                    someService.doSomeServiceStuff()
                }
            }
        ''')
        Method field = tester.getDeclaredMethod('doLogging')


        field.each {
            println it
        }
    }

}
