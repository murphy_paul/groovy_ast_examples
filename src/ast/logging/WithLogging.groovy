package ast.logging

import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Created with IntelliJ IDEA.
 * User: Paul.Murphy
 * Date: 9/19/13
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.METHOD])
@GroovyASTTransformationClass(['ast.logging.WithLoggingAstTransformation'])
public @interface WithLogging {
	String value() default "No message given"
}
