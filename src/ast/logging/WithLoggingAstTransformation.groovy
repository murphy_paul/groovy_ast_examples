package ast.logging

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

import static org.objectweb.asm.Opcodes.ACC_FINAL
import static org.objectweb.asm.Opcodes.ACC_PUBLIC


/**
 * Created with IntelliJ IDEA.
 * User: Paul.Murphy
 * Date: 9/19/13
 * Time: 1:30 PM
 * To change this template use File | Settings | File Templates.
 */
@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class WithLoggingAstTransformation implements ASTTransformation {

    private static final FILL_ME_IN = new ConstantExpression('fill_me_in')
    private static final OBJECT = new ClassNode(Object)
    private static final NO_EXCEPTIONS = [] as ClassNode[]
    private static final NO_PARAMS = [] as Parameter[]
    private static final NO_VARIABLE_SCOPE = null

    @Override
    public void visit(ASTNode[] nodes, SourceUnit sourceUnit) {

        AnnotationNode anode = nodes[0]

        if(anode == null) throw new Error("Null")

        String value = anode.getMember("value")?.value

        if(value == null || value.isEmpty()){
            //TODO should set error on the source unit
            throw new Error("No ttl value specified")
        }

        MethodNode node = nodes[1]

        buildMulToStatementMethod(node, value )

        println node

    }

    private buildMulToStatementMethod(MethodNode existing, String value){

//        BlockStatement block = new BlockStatement()
//        block.addStatement(es1)

        BlockStatement code = existing.getCode() as BlockStatement
//        code.getStatements().add(0, es1)
        if(code.getStatements() == null || code.getStatements().isEmpty()) return

        ExpressionStatement lastStatement = code.getStatements().last()

        if(lastStatement == null) return

        code.getStatements().remove(lastStatement)

        println lastStatement.text
        println lastStatement.getExpression().objectExpression
        println lastStatement.getExpression().method
        println lastStatement.getExpression().arguments


        ExpressionStatement es1 = new ExpressionStatement(new MethodCallExpression(
                new VariableExpression("this"),
                new ConstantExpression("someMethod"),
                new ArgumentListExpression(
                        lastStatement.getExpression(),
                        new ConstantExpression(value))
        ))
//
        code.addStatement(es1)

        println 'done'
//        block.addStatement(es2)
//
//        new MethodNode('addedAtCompileTime', ACC_PUBLIC, OBJECT, NO_PARAMS, NO_EXCEPTIONS, block)


    }

    private Statement createPrintlnStatement(String message){
        return new ExpressionStatement(
                new MethodCallExpression(
                        new VariableExpression("this"),
                        new ConstantExpression("println"),
                        new ArgumentListExpression(
                                new ConstantExpression(message)
                        )
                )
        )
    }
}
