package ast.logging

/**
 * Created with IntelliJ IDEA.
 * User: Paul.Murphy
 * Date: 9/19/13
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
class LoggingObject {

    SomeService someService = new SomeService()

    @WithLogging(value = "Hello")
    def addLoggingToThis(){
        someService.doSomeServiceStuff()
    }

    def testWhatItShouldLookLike(){
        someMethod( someService.doSomeServiceStuff(), "value")
    }


    def someMethod(result, value){
        println "result: $result"
        println "value: $value"
    }
}